import sys
import requests
from bs4 import BeautifulSoup

channel_id = sys.argv[1]
url = "https://www.youtube.com/feeds/videos.xml?channel_id=" + channel_id
html = requests.get(url)

soup = BeautifulSoup(html.text, 'lxml')

links = []
for entry in soup.find_all('entry'):
    for link in entry.find_all('link'):
        links.append(link['href'])

print(' '.join(links))
