scriptencoding utf-8

set background=dark

if v:version > 580
  hi clear
  if exists('syntax_on')
    syntax reset
  endif
endif

let g:colors_name='dycol'

if !has('gui_running') && &t_Co != 256
  finish
endif

" Palette {{{

let s:black          = ['#1c1b19', 0]
let s:red            = ['#ef2f27', 1]
let s:green          = ['#519f50', 2]
let s:yellow         = ['#fbb829', 3]
let s:blue           = ['#2c78bf', 4]
let s:magenta        = ['#e02c6d', 5]
let s:cyan           = ['#0aaeb3', 6]
let s:white          = ['#d0bfa1', 7]
let s:bright_black   = ['#918175', 8]
let s:bright_red     = ['#f75341', 9]
let s:bright_green   = ['#98bc37', 10]
let s:bright_yellow  = ['#fed06e', 11]
let s:bright_blue    = ['#68a8e4', 12]
let s:bright_magenta = ['#ff5c8f', 13]
let s:bright_cyan    = ['#53fde9', 14]
let s:bright_white   = ['#fce8c3', 15]

" xterm colors.
let s:orange        = ['#FF5F00', 202]
let s:bright_orange = ['#FF8700', 208]
let s:hard_black    = ['#121212', 233]
let s:xgray1        = ['#262626', 235]
let s:xgray2        = ['#303030', 236]
let s:xgray3        = ['#3A3A3A', 237]
let s:xgray4        = ['#444444', 238]
let s:xgray5        = ['#4E4E4E', 239]
let s:xgray6        = ['#585858', 240]

"}}}
" Setup Variables: {{{

let s:none = ['NONE', 'NONE']

if !exists('g:dycol_bold')
  let g:dycol_bold=1
endif

if !exists('g:dycol_italic')
  if has('gui_running') || $TERM_ITALICS ==? 'true'
    let g:dycol_italic=1
  else
    let g:dycol_italic=0
  endif
endif

if !exists('g:dycol_transparent_background')
  let g:dycol_transparent_background=0
endif

if !exists('g:dycol_undercurl')
  let g:dycol_undercurl=1
endif

if !exists('g:dycol_underline')
  let g:dycol_underline=1
endif

if !exists('g:dycol_inverse')
  let g:dycol_inverse=1
endif

if !exists('g:dycol_inverse_matches')
  let g:dycol_inverse_matches=0
endif

if !exists('g:dycol_inverse_match_paren')
  let g:dycol_inverse_match_paren=0
endif

if !exists('g:dycol_dim_lisp_paren')
  let g:dycol_dim_lisp_paren=0
endif

if !exists('g:dycol_guisp_fallback') || index(['fg', 'bg'], g:dycol_guisp_fallback) == -1
  let g:dycol_guisp_fallback='NONE'
endif

" }}}
" Setup Emphasis: {{{

let s:bold = 'bold,'
if g:dycol_bold == 0
  let s:bold = ''
endif

let s:italic = 'italic,'
if g:dycol_italic == 0
  let s:italic = ''
endif

let s:underline = 'underline,'
if g:dycol_underline == 0
  let s:underline = ''
endif

let s:undercurl = 'undercurl,'
if g:dycol_undercurl == 0
  let s:undercurl = ''
endif

let s:inverse = 'inverse,'
if g:dycol_inverse == 0
  let s:inverse = ''
endif

" }}}
" Highlighting Function: {{{

function! s:HL(group, fg, ...)
  " Arguments: group, guifg, guibg, gui, guisp

  " foreground
  let l:fg = a:fg

  " background
  if a:0 >= 1
    let l:bg = a:1
  else
    let l:bg = s:none
  endif

  " emphasis
  if a:0 >= 2 && strlen(a:2)
    let l:emstr = a:2
  else
    let l:emstr = 'NONE,'
  endif

  " special fallback
  if a:0 >= 3
    if g:dycol_guisp_fallback !=# 'NONE'
      let fg = a:3
    endif

    " bg fallback mode should invert higlighting
    if g:dycol_guisp_fallback ==# 'bg'
      let emstr .= 'inverse,'
    endif
  endif

  let l:histring = [ 'hi', a:group,
        \ 'guifg=' . l:fg[0], 'ctermfg=' . l:fg[1],
        \ 'guibg=' . l:bg[0], 'ctermbg=' . l:bg[1],
        \ 'gui=' . l:emstr[:-2], 'cterm=' . l:emstr[:-2]
        \ ]

  " special
  if a:0 >= 3
    call add(l:histring, 'guisp=' . a:3[0])
  endif

  execute join(l:histring, ' ')
endfunction

"}}}
" Dycol Hi Groups: {{{

" memoize common hi groups
call s:HL('DycolWhite', s:white)
call s:HL('DycolRed', s:red)
call s:HL('DycolGreen', s:green)
call s:HL('DycolYellow', s:yellow)
call s:HL('DycolBlue', s:blue)
call s:HL('DycolMagenta', s:magenta)
call s:HL('DycolCyan', s:cyan)
call s:HL('DycolBlack', s:black)

call s:HL('DycolRedBold', s:red, s:none, s:bold)
call s:HL('DycolGreenBold', s:green, s:none, s:bold)
call s:HL('DycolYellowBold', s:yellow, s:none, s:bold)
call s:HL('DycolBlueBold', s:blue, s:none, s:bold)
call s:HL('DycolMagentaBold', s:magenta, s:none, s:bold)
call s:HL('DycolCyanBold', s:cyan, s:none, s:bold)

call s:HL('DycolBrightRed', s:bright_red, s:none)
call s:HL('DycolBrightGreen', s:bright_green, s:none)
call s:HL('DycolBrightYellow', s:bright_yellow, s:none)
call s:HL('DycolBrightBlue', s:bright_blue, s:none)
call s:HL('DycolBrightMagenta', s:bright_magenta, s:none)
call s:HL('DycolBrightCyan', s:bright_cyan, s:none)
call s:HL('DycolBrightBlack', s:bright_black, s:none)
call s:HL('DycolBrightWhite', s:bright_white)

" special
call s:HL('DycolOrange', s:orange)
call s:HL('DycolBrightOrange', s:bright_orange)
call s:HL('DycolOrangeBold', s:orange, s:none, s:bold)
call s:HL('DycolHardBlack', s:hard_black)
call s:HL('DycolXgray1', s:xgray1)
call s:HL('DycolXgray2', s:xgray2)
call s:HL('DycolXgray3', s:xgray3)
call s:HL('DycolXgray4', s:xgray4)
call s:HL('DycolXgray5', s:xgray5)
call s:HL('DycolXgray6', s:xgray6)

" }}}
" Setup Terminal Colors For Neovim: {{{

if has('nvim')
  let g:terminal_color_0 = s:black[0]
  let g:terminal_color_8 = s:bright_black[0]

  let g:terminal_color_1 = s:red[0]
  let g:terminal_color_9 = s:bright_red[0]

  let g:terminal_color_2 = s:green[0]
  let g:terminal_color_10 = s:bright_green[0]

  let g:terminal_color_3 = s:yellow[0]
  let g:terminal_color_11 = s:bright_yellow[0]

  let g:terminal_color_4 = s:blue[0]
  let g:terminal_color_12 = s:bright_blue[0]

  let g:terminal_color_5 = s:magenta[0]
  let g:terminal_color_13 = s:bright_magenta[0]

  let g:terminal_color_6 = s:cyan[0]
  let g:terminal_color_14 = s:bright_cyan[0]

  let g:terminal_color_7 = s:white[0]
  let g:terminal_color_15 = s:bright_white[0]
endif

" }}}
" Setup Terminal Colors For Vim with termguicolors: {{{

if exists('*term_setansicolors')
  let g:terminal_ansi_colors = repeat([0], 16)

  let g:terminal_ansi_colors[0] = s:black[0]
  let g:terminal_ansi_colors[8] = s:bright_black[0]

  let g:terminal_ansi_colors[1] = s:red[0]
  let g:terminal_ansi_colors[9] = s:bright_red[0]

  let g:terminal_ansi_colors[2] = s:green[0]
  let g:terminal_ansi_colors[10] = s:bright_green[0]

  let g:terminal_ansi_colors[3] = s:yellow[0]
  let g:terminal_ansi_colors[11] = s:bright_yellow[0]

  let g:terminal_ansi_colors[4] = s:blue[0]
  let g:terminal_ansi_colors[12] = s:bright_blue[0]

  let g:terminal_ansi_colors[5] = s:magenta[0]
  let g:terminal_ansi_colors[13] = s:bright_magenta[0]

  let g:terminal_ansi_colors[6] = s:cyan[0]
  let g:terminal_ansi_colors[14] = s:bright_cyan[0]

  let g:terminal_ansi_colors[7] = s:white[0]
  let g:terminal_ansi_colors[15] = s:bright_white[0]
endif

" }}}

" Vanilla colorscheme ---------------------------------------------------------
" General UI: {{{

" Normal text
"
if g:dycol_transparent_background == 1 && !has('gui_running')
  call s:HL('Normal', s:bright_white, s:none)
 else
  call s:HL('Normal', s:bright_white, s:black)
endif

if v:version >= 700
  " Screen line that the cursor is
  call s:HL('CursorLine',   s:none, s:xgray2)
  " Screen column that the cursor is
  hi! link CursorColumn CursorLine

  call s:HL('TabLineFill', s:bright_black, s:xgray2)
  call s:HL('TabLineSel', s:bright_white, s:xgray5)

  " Not active tab page label
  hi! link TabLine TabLineFill

  " Match paired bracket under the cursor
  "
  if g:dycol_inverse_match_paren == 1
    call s:HL('MatchParen', s:bright_magenta, s:none, s:inverse . s:bold)
  else
    call s:HL('MatchParen', s:bright_magenta, s:none, s:bold)
  endif
endif

if v:version >= 703
  " Highlighted screen columns
  call s:HL('ColorColumn',  s:none, s:xgray2)

  " Concealed element: \lambda → λ
  call s:HL('Conceal', s:blue, s:none)

  " Line number of CursorLine
  if g:dycol_transparent_background == 1 && !has('gui_running')
    call s:HL('CursorLineNr', s:yellow, s:none)
  else
    call s:HL('CursorLineNr', s:yellow, s:black)
  endif

endif

hi! link NonText DycolXgray4
hi! link SpecialKey DycolBlue

if g:dycol_inverse == 1
  call s:HL('Visual', s:none, s:none, s:inverse)
else
  call s:HL('Visual', s:none, s:xgray2, s:bold)
endif

hi! link VisualNOS Visual

if g:dycol_inverse == 1 && g:dycol_inverse_matches == 1
  call s:HL('Search', s:none, s:none, s:inverse)
  call s:HL('IncSearch', s:none, s:none, s:inverse)
else
  call s:HL('Search', s:none, s:xgray5, s:bold)
  call s:HL('IncSearch', s:none, s:xgray5, s:underline . s:bold)
endif

call s:HL('Underlined', s:blue, s:none, s:underline)

call s:HL('StatusLine',   s:bright_white, s:xgray2)

if g:dycol_transparent_background == 1 && !has('gui_running')
  call s:HL('StatusLineNC', s:bright_black, s:none, s:underline)

  " The column separating vertically split windows
  call s:HL('VertSplit', s:bright_white, s:none)

  " Current match in wildmenu completion
  call s:HL('WildMenu', s:blue, s:none, s:bold)
else
  call s:HL('StatusLineNC', s:bright_black, s:black, s:underline)
  call s:HL('VertSplit', s:bright_white, s:black)
  call s:HL('WildMenu', s:blue, s:black, s:bold)
endif

" Directory names, special names in listing
hi! link Directory DycolGreenBold

" Titles for output from :set all, :autocmd, etc.
hi! link Title DycolGreenBold

" Error messages on the command line
call s:HL('ErrorMsg', s:bright_white, s:red)
" More prompt: -- More --
hi! link MoreMsg DycolYellowBold
" Current mode message: -- INSERT --
hi! link ModeMsg DycolYellowBold
" 'Press enter' prompt and yes/no questions
hi! link Question DycolOrangeBold
" Warning messages
hi! link WarningMsg DycolRedBold

" }}}
" Gutter: {{{

" Line number for :number and :# commands
call s:HL('LineNr', s:bright_black)

if g:dycol_transparent_background == 1 && !has('gui_running')
  " Column where signs are displayed
  " TODO Possibly need to fix  SignColumn
  call s:HL('SignColumn', s:none, s:none)
  " Line used for closed folds
  call s:HL('Folded', s:bright_black, s:none, s:italic)
  " Column where folds are displayed
  call s:HL('FoldColumn', s:bright_black, s:none)
else
  call s:HL('SignColumn', s:none, s:black)
  call s:HL('Folded', s:bright_black, s:black, s:italic)
  call s:HL('FoldColumn', s:bright_black, s:black)
endif

" }}}
" Cursor: {{{

" Character under cursor
call s:HL('Cursor', s:black, s:yellow)
" Visual mode cursor, selection
hi! link vCursor Cursor
" Input moder cursor
hi! link iCursor Cursor
" Language mapping cursor
hi! link lCursor Cursor

" }}}
" Syntax Highlighting: {{{

hi! link Special DycolOrange

call s:HL('Comment', s:bright_black, s:none, s:italic)

if g:dycol_transparent_background == 1 && !has('gui_running')
  call s:HL('Todo', s:bright_white, s:none, s:bold . s:italic)
else
  call s:HL('Todo', s:bright_white, s:black, s:bold . s:italic)
endif

call s:HL('Error', s:bright_white, s:red, s:bold)

" String constant: "this is a string"
call s:HL('String',  s:bright_green)

" Generic statement
hi! link Statement DycolRed
" if, then, else, endif, swicth, etc.
hi! link Conditional DycolRed
" for, do, while, etc.
hi! link Repeat DycolRed
" case, default, etc.
hi! link Label DycolRed
" try, catch, throw
hi! link Exception DycolRed
" sizeof, "+", "*", etc.
hi! link Operator Normal
" Any other keyword
hi! link Keyword DycolRed

" Variable name
hi! link Identifier DycolCyan
" Function name
hi! link Function DycolYellow

" Generic preprocessor
hi! link PreProc DycolCyan
" Preprocessor #include
hi! link Include DycolCyan
" Preprocessor #define
hi! link Define DycolCyan
" Same as Define
hi! link Macro DycolOrange
" Preprocessor #if, #else, #endif, etc.
hi! link PreCondit DycolCyan

" Generic constant
hi! link Constant DycolBrightMagenta
" Character constant: 'c', '/n'
hi! link Character DycolBrightMagenta
" Boolean constant: TRUE, false
hi! link Boolean DycolBrightMagenta
" Number constant: 234, 0xff
hi! link Number DycolBrightMagenta
" Floating point constant: 2.3e10
hi! link Float DycolBrightMagenta

" Generic type
if get(g:, 'dycol_italic_types', 0) == 1
  call s:HL('Type', s:bright_blue, s:none, s:italic)
else
  hi! link Type DycolBrightBlue
end
" static, register, volatile, etc
hi! link StorageClass DycolOrange
" struct, union, enum, etc.
hi! link Structure DycolCyan
" typedef
hi! link Typedef DycolMagenta

if g:dycol_dim_lisp_paren == 1
  hi! link Delimiter DycolXgray6
else
  hi! link Delimiter DycolBrightBlack
endif

" Treesitter
call s:HL('TSParameter', s:cyan, s:none, s:italic)

" }}}
" Completion Menu: {{{

if v:version >= 700
  " Popup menu: normal item
  call s:HL('Pmenu', s:bright_white, s:xgray2)
  " Popup menu: selected item
  call s:HL('PmenuSel', s:bright_white, s:blue, s:bold)

  if g:dycol_transparent_background == 1 && !has('gui_running')
    " Popup menu: scrollbar
    call s:HL('PmenuSbar', s:none, s:none)
    " Popup menu: scrollbar thumb
    call s:HL('PmenuThumb', s:none, s:none)
  else
    call s:HL('PmenuSbar', s:none, s:black)
    call s:HL('PmenuThumb', s:none, s:black)
  endif
endif

" }}}
" Diffs: {{{

if g:dycol_transparent_background == 1 && !has('gui_running')
  call s:HL('DiffDelete', s:red, s:none)
  call s:HL('DiffAdd',    s:green, s:none)
  call s:HL('DiffChange', s:cyan, s:none)
  call s:HL('DiffText',   s:yellow, s:none)
else
  call s:HL('DiffDelete', s:red, s:black)
  call s:HL('DiffAdd',    s:green, s:black)
  call s:HL('DiffChange', s:cyan, s:black)
  call s:HL('DiffText',   s:yellow, s:black)
endif

" }}}
" Spelling: {{{

if has('spell')
  " Not capitalised word, or compile warnings
  call s:HL('SpellCap',   s:green, s:none, s:bold . s:italic)
  " Not recognized word
  call s:HL('SpellBad',   s:none, s:none, s:undercurl, s:blue)
  " Wrong spelling for selected region
  call s:HL('SpellLocal', s:none, s:none, s:undercurl, s:cyan)
  " Rare word
  call s:HL('SpellRare',  s:none, s:none, s:undercurl, s:magenta)
endif

" }}}
" Terminal: {{{

if has('terminal')
  " Must set an explicit background as NONE won't work
  " Therefore not useful with transparent background option
  call s:HL('Terminal', s:bright_white, s:hard_black)
endif

" }}}
" Neovim's builtin LSP: {{{

hi! link LspDiagnosticsDefaultError DycolBrightRed
hi! link LspDiagnosticsDefaultWarning DycolBrightYellow
hi! link LspDiagnosticsDefaultInformation DycolBrightGreen
hi! link LspDiagnosticsDefaultHint DycolBrightCyan
call s:HL('LspDiagnosticsUnderlineError', s:bright_red, s:none, s:underline)
call s:HL('LspDiagnosticsUnderlineWarning', s:bright_yellow, s:none, s:underline)
call s:HL('LspDiagnosticsUnderlineInformation', s:bright_green, s:none, s:underline)
call s:HL('LspDiagnosticsUnderlineHint', s:bright_cyan, s:none, s:underline)

" }}}

" Plugin specific -------------------------------------------------------------
" Sneak: {{{

hi! link Sneak Search
call s:HL('SneakScope', s:none, s:hard_black)
hi! link SneakLabel Search

" }}}
" Rainbow Parentheses: {{{

if !exists('g:rbpt_colorpairs')
  let g:rbpt_colorpairs =
    \ [
      \ ['blue',  '#2C78BF'], ['202',  '#FF5F00'],
      \ ['red',  '#EF2F27'], ['magenta', '#E02C6D']
    \ ]
endif

let g:rainbow_guifgs = [ '#E02C6D', '#EF2F27', '#D75F00', '#2C78BF']
let g:rainbow_ctermfgs = [ 'magenta', 'red', '202', 'blue' ]

if !exists('g:rainbow_conf')
  let g:rainbow_conf = {}
endif
if !has_key(g:rainbow_conf, 'guifgs')
  let g:rainbow_conf['guifgs'] = g:rainbow_guifgs
endif
if !has_key(g:rainbow_conf, 'ctermfgs')
  let g:rainbow_conf['ctermfgs'] = g:rainbow_ctermfgs
endif

let g:niji_dark_colours = g:rbpt_colorpairs
let g:niji_light_colours = g:rbpt_colorpairs

"}}}
" GitGutter: {{{

hi! link GitGutterAdd DycolGreen
hi! link GitGutterChange DycolYellow
hi! link GitGutterDelete DycolRed
hi! link GitGutterChangeDelete DycolYellow

" }}}
" GitCommit: "{{{

hi! link gitcommitSelectedFile DycolGreen
hi! link gitcommitDiscardedFile DycolRed

" }}}
" Asynchronous Lint Engine: {{{

call s:HL('ALEError', s:none, s:none, s:undercurl, s:red)
call s:HL('ALEWarning', s:none, s:none, s:undercurl, s:yellow)
call s:HL('ALEInfo', s:none, s:none, s:undercurl, s:blue)

hi! link ALEErrorSign DycolRed
hi! link ALEWarningSign DycolYellow
hi! link ALEInfoSign DycolBlue

" }}}
" vim-indent-guides: {{{

call s:HL('IndentGuidesEven', s:none, s:xgray3)
call s:HL('IndentGuidesOdd',  s:none, s:xgray4)

" }}}
" vim-startify {{{

hi! link StartifyNumber Statement
hi! link StartifyFile Normal
hi! link StartifyPath String
hi! link StartifySlash Normal
hi! link StartifyBracket Comment
hi! link StartifyHeader Type
hi! link StartifyFooter Normal
hi! link StartifySpecial Comment
hi! link StartifySection Identifier

" }}}
" fzf: {{{

call s:HL('fzf1', s:magenta, s:xgray2)
call s:HL('fzf2', s:bright_green, s:xgray2)
call s:HL('fzf3', s:bright_white, s:xgray2)

"}}}
" Netrw: {{{

hi! link netrwDir DycolBlue
hi! link netrwClassify DycolCyan
hi! link netrwLink DycolBrightBlack
hi! link netrwSymLink DycolCyan
hi! link netrwExe DycolYellow
hi! link netrwComment DycolBrightBlack
hi! link netrwList DycolBrightBlue
hi! link netrwTreeBar DycolBrightBlack
hi! link netrwHelpCmd DycolCyan
hi! link netrwVersion DycolGreen
hi! link netrwCmdSep DycolBrightBlack

"}}}
" coc.nvim: {{{

hi! link CocErrorSign DycolRed
hi! link CocWarningSign DycolBrightOrange
hi! link CocInfoSign DycolYellow
hi! link CocHintSign DycolBlue
hi! link CocErrorFloat DycolRed
hi! link CocWarningFloat DycolOrange
hi! link CocInfoFloat DycolYellow
hi! link CocHintFloat DycolBlue
hi! link CocDiagnosticsError DycolRed
hi! link CocDiagnosticsWarning DycolOrange
hi! link CocDiagnosticsInfo DycolYellow
hi! link CocDiagnosticsHint DycolBlue

hi! link CocSelectedText DycolRed
hi! link CocCodeLens DycolWhite

call s:HL('CocErrorHighlight', s:none, s:none, s:undercurl, s:red)
call s:HL('CocWarningHighlight', s:none, s:none, s:undercurl, s:bright_orange)
call s:HL('CocInfoHighlight', s:none, s:none, s:undercurl, s:yellow)
call s:HL('CocHintHighlight', s:none, s:none, s:undercurl, s:blue)

" }}}
" CtrlP: "{{{
"
hi! link CtrlPMatch DycolMagenta
hi! link CtrlPLinePre DycolBrightGreen
call s:HL('CtrlPMode1', s:bright_white, s:xgray3)
call s:HL('CtrlPMode2', s:bright_white, s:xgray5)
call s:HL('CtrlPStats', s:yellow, s:xgray2)

" }}}
" NERDTree: "{{{

hi! link NERDTreeDir DycolBlue
hi! link NERDTreeDirSlash DycolCyan
hi! link NERDTreeOpenable DycolBlue
hi! link NERDTreeClosable DycolBlue
hi! link NERDTreeFile DycolWhite
hi! link NERDTreeExecFile DycolYellow
hi! link NERDTreeUp DycolOrange
hi! link NERDTreeCWD DycolGreen
hi! link NERDTreeHelp DycolCyan
hi! link NERDTreeFlags DycolCyan
hi! link NERDTreeLinkFile DycolBrightBlack
hi! link NERDTreeLinkTarget DycolBrightBlack

" }}}
" Telescope: "{{{

call s:HL('TelescopeNormal', s:white, s:none)
call s:HL('TelescopeSelection', s:green, s:none, s:bold)
call s:HL('TelescopeMatching', s:magenta)
call s:HL('TelescopeSelectionCaret', s:magenta)
call s:HL('TelescopePromptPrefix', s:bright_yellow)

" }}}

" Filetype specific -----------------------------------------------------------
" Diff: {{{

hi! link diffAdded DycolGreen
hi! link diffRemoved DycolRed
hi! link diffChanged DycolCyan

hi! link diffFile DycolOrange
hi! link diffNewFile DycolYellow

hi! link diffLine DycolBlue

" }}}
" Html: {{{

hi! link htmlTag DycolBlue
hi! link htmlEndTag DycolBlue

hi! link htmlTagName DycolBlue
hi! link htmlTag DycolBrightBlack
hi! link htmlArg DycolYellow

hi! link htmlScriptTag DycolRed
hi! link htmlTagN DycolBlue
hi! link htmlSpecialTagName DycolBlue

call s:HL('htmlLink', s:bright_white, s:none, s:underline)

hi! link htmlSpecialChar DycolYellow

if g:dycol_transparent_background == 1 && !has('gui_running')
  call s:HL('htmlBold', s:bright_white, s:none, s:bold)
  call s:HL('htmlBoldUnderline', s:bright_white, s:none, s:bold . s:underline)
  call s:HL('htmlBoldItalic', s:bright_white, s:none, s:bold . s:italic)
  call s:HL('htmlBoldUnderlineItalic', s:bright_white, s:none, s:bold . s:underline . s:italic)
  call s:HL('htmlUnderline', s:bright_white, s:none, s:underline)
  call s:HL('htmlUnderlineItalic', s:bright_white, s:none, s:underline . s:italic)
  call s:HL('htmlItalic', s:bright_white, s:none, s:italic)
else
  call s:HL('htmlBold', s:bright_white, s:black, s:bold)
  call s:HL('htmlBoldUnderline', s:bright_white, s:black, s:bold . s:underline)
  call s:HL('htmlBoldItalic', s:bright_white, s:black, s:bold . s:italic)
  call s:HL('htmlBoldUnderlineItalic', s:bright_white, s:black, s:bold . s:underline . s:italic)
  call s:HL('htmlUnderline', s:bright_white, s:black, s:underline)
  call s:HL('htmlUnderlineItalic', s:bright_white, s:black, s:underline . s:italic)
  call s:HL('htmlItalic', s:bright_white, s:black, s:italic)
endif

" }}}
" Xml: {{{

hi! link xmlTag DycolBlue
hi! link xmlEndTag DycolBlue
hi! link xmlTagName DycolBlue
hi! link xmlEqual DycolBlue
hi! link docbkKeyword DycolCyanBold

hi! link xmlDocTypeDecl DycolBrightBlack
hi! link xmlDocTypeKeyword DycolMagenta
hi! link xmlCdataStart DycolBrightBlack
hi! link xmlCdataCdata DycolMagenta
hi! link dtdFunction DycolBrightBlack
hi! link dtdTagName DycolMagenta

hi! link xmlAttrib DycolCyan
hi! link xmlProcessingDelim DycolBrightBlack
hi! link dtdParamEntityPunct DycolBrightBlack
hi! link dtdParamEntityDPunct DycolBrightBlack
hi! link xmlAttribPunct DycolBrightBlack

hi! link xmlEntity DycolYellow
hi! link xmlEntityPunct DycolYellow

" }}}
" Vim: {{{

call s:HL('vimCommentTitle', s:bright_white, s:none, s:bold . s:italic)

hi! link vimNotation DycolYellow
hi! link vimBracket DycolYellow
hi! link vimMapModKey DycolYellow
hi! link vimFuncSID DycolBrightWhite
hi! link vimSetSep DycolBrightWhite
hi! link vimSep DycolBrightWhite
hi! link vimContinue DycolBrightWhite

" }}}
" Lisp dialects: {{{

if g:dycol_dim_lisp_paren == 1
  hi! link schemeParentheses DycolXgray6
  hi! link clojureParen DycolXgray6
else
  hi! link schemeParentheses DycolBrightBlack
  hi! link clojureParen DycolBrightBlack
endif

hi! link clojureKeyword DycolBlue
hi! link clojureCond DycolRed
hi! link clojureSpecial DycolRed
hi! link clojureDefine DycolRed

hi! link clojureFunc DycolYellow
hi! link clojureRepeat DycolYellow
hi! link clojureCharacter DycolCyan
hi! link clojureStringEscape DycolCyan
hi! link clojureException DycolRed

hi! link clojureRegexp DycolCyan
hi! link clojureRegexpEscape DycolCyan
call s:HL('clojureRegexpCharClass', s:bright_white, s:none, s:bold)
hi! link clojureRegexpMod clojureRegexpCharClass
hi! link clojureRegexpQuantifier clojureRegexpCharClass

hi! link clojureAnonArg DycolYellow
hi! link clojureVariable DycolBlue
hi! link clojureMacro DycolOrangeBold

hi! link clojureMeta DycolYellow
hi! link clojureDeref DycolYellow
hi! link clojureQuote DycolYellow
hi! link clojureUnquote DycolYellow

" }}}
" C: {{{

hi! link cOperator DycolMagenta
hi! link cStructure DycolYellow

" }}}
" Python: {{{

hi! link pythonBuiltin DycolYellow
hi! link pythonBuiltinObj DycolYellow
hi! link pythonBuiltinFunc DycolYellow
hi! link pythonFunction DycolCyan
hi! link pythonDecorator DycolRed
hi! link pythonInclude DycolBlue
hi! link pythonImport DycolBlue
hi! link pythonRun DycolBlue
hi! link pythonCoding DycolBlue
hi! link pythonOperator DycolRed
hi! link pythonExceptions DycolMagenta
hi! link pythonBoolean DycolMagenta
hi! link pythonDot DycolBrightWhite

" }}}
" CSS/SASS: {{{

hi! link cssBraces DycolBrightWhite
hi! link cssFunctionName DycolYellow
hi! link cssIdentifier DycolBlue
hi! link cssClassName DycolBlue
hi! link cssClassNameDot DycolBlue
hi! link cssColor DycolBrightMagenta
hi! link cssSelectorOp DycolBlue
hi! link cssSelectorOp2 DycolBlue
hi! link cssImportant DycolGreen
hi! link cssVendor DycolBlue
hi! link cssMediaProp DycolYellow
hi! link cssBorderProp DycolYellow
hi! link cssAttrComma DycolBrightWhite

hi! link cssTextProp DycolYellow
hi! link cssAnimationProp DycolYellow
hi! link cssUIProp DycolYellow
hi! link cssTransformProp DycolYellow
hi! link cssTransitionProp DycolYellow
hi! link cssPrintProp DycolYellow
hi! link cssPositioningProp DycolYellow
hi! link cssBoxProp DycolYellow
hi! link cssFontDescriptorProp DycolYellow
hi! link cssFlexibleBoxProp DycolYellow
hi! link cssBorderOutlineProp DycolYellow
hi! link cssBackgroundProp DycolYellow
hi! link cssMarginProp DycolYellow
hi! link cssListProp DycolYellow
hi! link cssTableProp DycolYellow
hi! link cssFontProp DycolYellow
hi! link cssPaddingProp DycolYellow
hi! link cssDimensionProp DycolYellow
hi! link cssRenderProp DycolYellow
hi! link cssColorProp DycolYellow
hi! link cssGeneratedContentProp DycolYellow
hi! link cssTagName DycolBrightBlue

" SASS
hi! link sassClass DycolBlue
hi! link sassClassChar DycolBlue
hi! link sassVariable DycolCyan
hi! link sassIdChar DycolBrightBlue

" }}}
" JavaScript: {{{

hi! link javaScriptMember DycolBlue
hi! link javaScriptNull DycolMagenta

" }}}
" YAJS: {{{

hi! link javascriptParens DycolBrightCyan
hi! link javascriptFuncArg Normal
hi! link javascriptDocComment DycolGreen
hi! link javascriptArrayMethod Function
hi! link javascriptReflectMethod Function
hi! link javascriptStringMethod Function
hi! link javascriptObjectMethod Function
hi! link javascriptObjectStaticMethod Function
hi! link javascriptObjectLabel DycolBlue

hi! link javascriptProp DycolBlue

hi! link javascriptVariable DycolBrightBlue
hi! link javascriptOperator DycolBrightCyan
hi! link javascriptFuncKeyword DycolBrightRed
hi! link javascriptFunctionMethod DycolYellow
hi! link javascriptReturn DycolBrightRed
hi! link javascriptEndColons Normal

" }}}
" CoffeeScript: {{{

hi! link coffeeExtendedOp DycolBrightWhite
hi! link coffeeSpecialOp DycolBrightWhite
hi! link coffeeCurly DycolYellow
hi! link coffeeParen DycolBrightWhite
hi! link coffeeBracket DycolYellow

" }}}
" Ruby: {{{

hi! link rubyStringDelimiter DycolGreen
hi! link rubyInterpolationDelimiter DycolCyan
hi! link rubyDefine Keyword

" }}}
" ObjectiveC: {{{

hi! link objcTypeModifier DycolRed
hi! link objcDirective DycolBlue

" }}}
" Go: {{{

hi! link goDirective DycolCyan
hi! link goConstants DycolMagenta
hi! link goDeclaration DycolRed
hi! link goDeclType DycolBlue
hi! link goBuiltins DycolYellow

" }}}
" Lua: {{{

hi! link luaIn DycolRed
hi! link luaFunction DycolCyan
hi! link luaTable DycolYellow

" }}}
" MoonScript: {{{

hi! link moonSpecialOp DycolBrightWhite
hi! link moonExtendedOp DycolBrightWhite
hi! link moonFunction DycolBrightWhite
hi! link moonObject DycolYellow

" }}}
" Java: {{{

hi! link javaAnnotation DycolBlue
hi! link javaDocTags DycolCyan
hi! link javaCommentTitle vimCommentTitle
hi! link javaParen DycolBrightWhite
hi! link javaParen1 DycolBrightWhite
hi! link javaParen2 DycolBrightWhite
hi! link javaParen3 DycolBrightWhite
hi! link javaParen4 DycolBrightWhite
hi! link javaParen5 DycolBrightWhite
hi! link javaOperator DycolYellow

hi! link javaVarArg DycolGreen

" }}}
" Elixir: {{{

hi! link elixirDocString Comment

hi! link elixirStringDelimiter DycolGreen
hi! link elixirInterpolationDelimiter DycolCyan

" }}}
" Scala: {{{

" NB: scala vim syntax file is kinda horrible
hi! link scalaNameDefinition DycolBlue
hi! link scalaCaseFollowing DycolBlue
hi! link scalaCapitalWord DycolBlue
hi! link scalaTypeExtension DycolBlue

hi! link scalaKeyword DycolRed
hi! link scalaKeywordModifier DycolRed

hi! link scalaSpecial DycolCyan
hi! link scalaOperator DycolBlue

hi! link scalaTypeDeclaration DycolYellow
hi! link scalaTypeTypePostDeclaration DycolYellow

hi! link scalaInstanceDeclaration DycolBlue
hi! link scalaInterpolation DycolCyan

" }}}
" Markdown: {{{

call s:HL('markdownItalic', s:bright_white, s:none, s:italic)

hi! link markdownH1 DycolGreenBold
hi! link markdownH2 DycolGreenBold
hi! link markdownH3 DycolYellowBold
hi! link markdownH4 DycolYellowBold
hi! link markdownH5 DycolYellow
hi! link markdownH6 DycolYellow

hi! link markdownCode DycolCyan
hi! link markdownCodeBlock DycolCyan
hi! link markdownCodeDelimiter DycolCyan

hi! link markdownBlockquote DycolBrightBlack
hi! link markdownListMarker DycolBrightBlack
hi! link markdownOrderedListMarker DycolBrightBlack
hi! link markdownRule DycolBrightBlack
hi! link markdownHeadingRule DycolBrightBlack

hi! link markdownUrlDelimiter DycolBrightWhite
hi! link markdownLinkDelimiter DycolBrightWhite
hi! link markdownLinkTextDelimiter DycolBrightWhite

hi! link markdownHeadingDelimiter DycolYellow
hi! link markdownUrl DycolMagenta
hi! link markdownUrlTitleDelimiter DycolGreen

call s:HL('markdownLinkText', s:bright_black, s:none, s:underline)
hi! link markdownIdDeclaration markdownLinkText

" }}}
" Haskell: {{{

" hi! link haskellType DycolYellow
" hi! link haskellOperators DycolYellow
" hi! link haskellConditional DycolCyan
" hi! link haskellLet DycolYellow

hi! link haskellType DycolBlue
hi! link haskellIdentifier DycolBlue
hi! link haskellSeparator DycolBlue
hi! link haskellDelimiter DycolBrightWhite
hi! link haskellOperators DycolBlue

hi! link haskellBacktick DycolYellow
hi! link haskellStatement DycolYellow
hi! link haskellConditional DycolYellow

hi! link haskellLet DycolCyan
hi! link haskellDefault DycolCyan
hi! link haskellWhere DycolCyan
hi! link haskellBottom DycolCyan
hi! link haskellBlockKeywords DycolCyan
hi! link haskellImportKeywords DycolCyan
hi! link haskellDeclKeyword DycolCyan
hi! link haskellDeriving DycolCyan
hi! link haskellAssocType DycolCyan

hi! link haskellNumber DycolMagenta
hi! link haskellPragma DycolMagenta

hi! link haskellString DycolGreen
hi! link haskellChar DycolGreen

" }}}
" Json: {{{

hi! link jsonKeyword DycolGreen
hi! link jsonQuote DycolGreen
hi! link jsonBraces DycolBlue
hi! link jsonString DycolBlue

" }}}
" Rust: {{{

"https://github.com/rust-lang/rust.vim/blob/master/syntax/rust.vim
hi! link rustCommentLineDoc DycolGreen
hi! link rustModPathSep DycolBrightBlack

" }}}
" Make: {{{

hi! link makePreCondit DycolRed
hi! link makeCommands DycolBrightWhite
hi! link makeTarget DycolYellow

" }}}
" Misc: {{{

call s:HL('shParenError', s:bright_white, s:bright_red)
call s:HL('ExtraWhitespace', s:none, s:red)

" }}}

" vim: set sw=2 ts=2 sts=2 et tw=80 ft=vim fdm=marker :
