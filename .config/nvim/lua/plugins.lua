vim.cmd [[packadd packer.nvim]]
vim.cmd [[autocmd BufWritePost plugins.lua PackerCompile]]

return require("packer").startup(function()
	use { "wbthomason/packer.nvim", opt = true }

	use { "godlygeek/tabular" }
	use { "hrsh7th/vim-vsnip",
		config="require('plugins.snippets')"
	}
	use { "hrsh7th/vim-vsnip-integ" }
	use { "hoob3rt/lualine.nvim",
		config="require('plugins.line')"
	}
	use { "junegunn/fzf" }
	use { "junegunn/fzf.vim",
		config="require('plugins.fzf')"
	}
	use { "junegunn/goyo.vim" }
	use { "junegunn/limelight.vim" }
	use { "jiangmiao/auto-pairs",
		config="require('plugins.autopairs')"
	}
	use { "kyazdani42/nvim-tree.lua",
		config="require('plugins.tree')"
	}
	use { "lewis6991/gitsigns.nvim",
		requires = { "nvim-lua/plenary.nvim" },
		config="require('plugins.gitsigns')",
	}
	use { "liuchengxu/vista.vim",
		config="require('plugins.vista')"
	}
	use { "kyazdani42/nvim-web-devicons" }
	use { "mbbill/undotree" }
	use { "neovim/nvim-lspconfig",
		config="require('plugins.lsp')"
	}
	use { "npxbr/glow.nvim",
		run=":GlowInstall"
	} --had to run manually, nightly issue?
	use { "norcalli/nvim-colorizer.lua",
		config="require('plugins.colorizer')"
	}
	use { "nvim-lua/completion-nvim",
		config="require('plugins.completion')"
	}
	use { "nvim-telescope/telescope.nvim",
		config = "require('plugins.telescope')",
		requires = {{"nvim-lua/popup.nvim"}, {"nvim-lua/plenary.nvim"}}
	}
	use { "nvim-telescope/telescope-packer.nvim" }
	use { "nvim-treesitter/nvim-treesitter",
		run=":TSUpdate",
		config="require('plugins.treesitter')"
	}
	use { "plasticboy/vim-markdown",
		config="require('plugins.markdown')"
	}
	use { "rrethy/vim-illuminate" }
	use { "tpope/vim-commentary" }
	use { "tpope/vim-surround" }
	-- use { "vijaymarupudi/nvim-fzf" }
	-- use { "vijaymarupudi/nvim-fzf-commands" }
	use { "vimwiki/vimwiki",
		branch="dev",
		requires={"tools-life/taskwiki"},
		config="require('plugins.vimwiki')"
	}
end)
