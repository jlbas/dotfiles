local fn = vim.fn
local exe = vim.api.nvim_command

M = {}

function M.toggle()
	if (fn.winnr('$') > 1) then
		exe('silent tab split')
	elseif (fn.tabpagenr() > 1) then
		exe('silent tabclose')
	end
end

return M
