local d = require("dycolors")

local line = {}

local colors = {
  bg0 = d.black,
  bg1 = d.gray_dark,
  bg2 = d.gray_darker,
  fg1 = d.gray_light,
  fg2 = d.gray,
  col_i = d.gray,
  col_c = d.gray,
  col_n = d.gray,
  col_r = d.gray,
  col_t = d.gray,
  col_v = d.gray,
}

line.normal = {
  a = {
    bg = colors.col_n,
    fg = colors.fg1,
  },
  b = {
    bg = colors.bg2,
    fg = colors.fg1,
  },
  c = {
    bg = colors.bg1,
    fg = colors.fg1,
  },
}

line.insert = {
  a = {
    bg = colors.col_i,
    fg = colors.fg1,
  },
  b = {
    bg = colors.bg2,
    fg = colors.fg1,
  },
  c = {
    bg = colors.bg1,
    fg = colors.fg1,
  },
}

line.visual = {
  a = {
    bg = colors.col_v,
    fg = colors.fg1,
  },
  b = {
    bg = colors.bg2,
    fg = colors.fg1,
  },
  c = {
    bg = colors.bg1,
    fg = colors.fg1,
  },
}

line.replace = {
  a = {
    bg = colors.col_r,
    fg = colors.fg1,
  },
  b = {
    bg = colors.bg2,
    fg = colors.fg1,
  },
  c = {
    bg = colors.bg1,
    fg = colors.fg1,
  },
}

line.command = {
  a = {
    bg = colors.col_c,
    fg = colors.fg1,
  },
  b = {
    bg = colors.bg2,
    fg = colors.fg1,
  },
  c = {
    bg = colors.bg1,
    fg = colors.fg1,
  },
}

line.terminal = {
  a = {
    bg = colors.col_t,
    fg = colors.fg1,
  },
  b = {
    bg = colors.bg2,
    fg = colors.fg1,
  },
  c = {
    bg = colors.bg1,
    fg = colors.fg1,
  },
}

line.inactive = {
  a = {
    bg = colors.bg1,
    fg = colors.fg1,
  },
  b = {
    bg = colors.bg1,
    fg = colors.fg2,
  },
  c = {
    bg = colors.bg1,
    fg = colors.fg2,
  },
}

return line
