local H = require "helper"

local options = {
	background = 'dark',
	cursorline = true,
	conceallevel = 0,
	hidden = true,
	ignorecase = true,
	iskeyword = {'-', '+='},
	nofoldenable = true,
	number = true,
	relativenumber = true,
	shiftwidth = 4,
	showmode = false,
	smartcase = true,
	smartindent = true,
	showtabline = 0,
	spellfile = '/home/jl/.config/nvim/spell/en.utf-8.add',
	splitbelow = true,
	splitright = true,
	tabstop = 4,
	termguicolors = true,
	undofile = true,
}

H.set_options(options)
vim.cmd('let &t_SI = "\\<Esc>[6 q"')
vim.cmd('let &t_SR = "\\<Esc>[4 q"')
vim.cmd('let &t_EI = "\\<Esc>[2 q"')
vim.cmd('colorscheme dycol')
vim.cmd('noswapfile')
vim.cmd('filetype plugin indent on')
vim.cmd('syntax enable')
