local H = {}

function H.set_options(options)
	for key, val in pairs(options) do
		if type(val) == "table" then
			vim.api.nvim_command('set ' .. key .. val[2] .. val[1])
		elseif val == true then
			vim.api.nvim_command('set ' .. key)
		elseif val == false then
			vim.api.nvim_command('set no' .. key)
		else
			vim.api.nvim_command('set ' .. key .. '=' .. val)
		end
	end
end

function H.map(mode, lhs, rhs, opts)
	opts = opts or {}
	if opts['noremap']==nil then
		opts = vim.tbl_extend('force', { noremap = true }, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, opts)
end

return H
