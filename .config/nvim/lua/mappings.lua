vim.g.mapleader = " "

local H = require "helper"

----- General -----

--Pane nav/sizing
H.map('n', '<C-h>', "<C-w>h")
H.map('n', '<C-j>', "<C-w>j")
H.map('n', '<C-k>', "<C-w>k")
H.map('n', '<C-l>', "<C-w>l")
H.map('n', '<M-h>', ":vertical resize +5<CR>")
H.map('n', '<M-j>', ":resize -5<CR>")
H.map('n', '<M-k>', ":resize +5<CR>")
H.map('n', '<M-l>', ":vertical resize -5<CR>")

--Terminal
H.map('n', '<leader>x', ":lua require('terminal').new('h')<CR>")
H.map('n', '<leader>v', ":lua require('terminal').new('v')<CR>")

--Toggle fullscreen
H.map('n', '<leader>z', ":lua require('fullscreen').toggle()<CR>", {silent = true})

--Buffer navigation
H.map('n', '<leader>]', ":bnext<CR>", {silent = true})
H.map('n', '<leader>[', ":bprevious<CR>", {silent = true})

--Visual char delimiter
H.map('n', '<leader>cc', ":let &cc = &cc == '' ? '120' : ''<CR>", {silent = true})

--Change to directory of current buffer
H.map('n', '<leader>cd', ":lcd %:p:h<CR>", {silent = true})

--Diagnostics
H.map('n', '<leader>dn', ":lua vim.lsp.diagnostic.goto_next()<CR>")
H.map('n', '<leader>dp', ":lua vim.lsp.diagnostic.goto_prev()<CR>")

--Turn off highlight
H.map('n', '<leader>h', ":nohlsearch<CR>")

--Get highlight group under cursor
H.map('n', '<leader>i', ":echo \"hi<\" . synIDattr(synID(line(\".\"),col(\".\"),1),\"name\") . '> trans<' synIDattr(synID(line(\".\"),col(\".\"),0),\"name\") . \"> lo<\" synIDattr(synIDtrans(synID(line(\".\"),col(\".\"),1)),\"name\") . \">\"<CR>")

--Generate PDF from Markdown file
H.map('n', '<leader>md', ":execute 'silent !pandoc -V geometry:margin=2cm -o %:r.pdf %' <bar> :execute 'redraw!'<CR>")

--System clipboard
H.map('n', '<leader>p', "\"+p")
H.map('n', '<leader>P', "\"+P")
H.map('v', '<leader>p', "\"+p")
H.map('v', '<leader>P', "\"+P")
H.map('n', '<leader>Y', "\"+Y")
H.map('v', '<leader>y', "\"+y")

--Spelling
H.map('i', '<C-s>', "<Esc>mb[s1z=`ba")
H.map('n', '<C-s>', "mb[s1z=`b")

--Faster quit
H.map('n', '<leader>s', ":w<CR>")
H.map('n', '<leader>q', ":wq!<CR>")
H.map('n', '<leader><S-q>', ":q!<CR>")

--Better indenting
H.map('v', '<', "<gv")
H.map('v', '>', ">gv")

----- PLUGINS -----

--Colorizer
H.map('n', '<leader>c', ":ColorizerToggle<CR>")

--Completion
H.map('i', '<Tab>', 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', {expr = true})
H.map('i', '<S-Tab>', 'pumvisible() ? "\\<C-p>" : "\\<S-Tab>"', {expr = true})

--fzf
H.map('n', '<leader>ff', ":Files<CR>" )
H.map('n', '<leader>fa', ":Files ~<CR>" )
H.map('n', '<leader>fb', ":Buffers<CR>" )
H.map('n', '<leader>fg', ":Rg<CR>" )
H.map('n', '<leader>ft', ":Tags<CR>" )
H.map('n', '<leader>f:', ":History:<CR>" )
H.map('n', '<leader>fc', ":Commands<CR>" )
H.map('n', '<leader>fh', ":Helptags<CR>" )

--Goyo
H.map('n', '<leader>g', ":Goyo<CR>:Limelight!!<CR>")
H.map('n', '<leader>m', ":Glow<CR>")

--nvim-tree
H.map('n', '<leader>n', ":NvimTreeToggle<CR>")

--Tabular
H.map('n', '<leader>a', ":Tabular /")

--Telescope
-- H.map('n', '<leader>fb', "<cmd>Telescope buffers<CR>")
-- H.map('n', '<leader>fc', "<cmd>Telescope commands<CR>")
-- H.map('n', '<leader>ff', "<cmd>Telescope find_files<CR>")
-- H.map('n', '<leader>fg', "<cmd>Telescope live_grep<CR>")
-- H.map('n', '<leader>fh', "<cmd>Telescope help_tags<CR>")
-- H.map('n', '<leader>fm', "<cmd>Telescope man_pages<CR>")
-- H.map('n', '<leader>ft', "<cmd>Telescope treesitter<CR>")
-- H.map('n', '<leader>fr', "<cmd>Telescope registers<CR>")
-- H.map('n', '<leader>fv', "<cmd>Telescope vim_options<CR>")
-- H.map('n', '<leader>fp', "<cmd>lua require('telescope').extensions.packer.plugins(opts)<CR>")

--UndoTree
H.map('n', '<leader>u', ":UndotreeToggle<CR>")

--Vimwiki
H.map('n', '<leader>ws', "<Plug>VimwikiSplitLink", {noremap = false})
H.map('n', '<leader>wv', "<Plug>VimwikiVSplitLink", {noremap = false})

--Vista
H.map('n', '<leader>t', ":Vista!!<CR>")

--Vsnip
H.map('i', '<c-j>', [[vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-j>']], {expr = true, noremap = false})
H.map('s', '<c-j>', [[vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-j>']], {expr = true, noremap = false})
H.map('x', '<c-j>', [[vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-j>']], {expr = true, noremap = false})
H.map('i', '<c-l>', [[vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<c-l>']], {expr = true, noremap = false})
H.map('s', '<c-l>', [[vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<c-l>']], {expr = true, noremap = false})
H.map('i', '<c-h>', [[vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<c-h>']], {expr = true, noremap = false})
H.map('s', '<c-h>', [[vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<c-h>']], {expr = true, noremap = false})
