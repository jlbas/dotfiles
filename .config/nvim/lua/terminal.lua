local exec = vim.api.nvim_command
local M = {}
local opts = { "nonumber", "norelativenumber" }
local t_esc = "<C-\\><C-n>"
local w_cmd = t_esc .. "<C-w>"

local dirs_tbl =
{
	h = "sp | terminal",
	v = "vsp | terminal"
}

local tmaps = {
	{'<leader>jk', t_esc },
	{'<leader>kj', t_esc },
	{'<leader>x', t_esc .. ":lua require('terminal').new('h')<CR>"},
	{'<leader>v', t_esc .. ":lua require('terminal').new('v')<CR>"},
	{'<leader><Esc>', t_esc .. ":q<CR>"},
	{'<leader>w', t_esc .. ":bd! term://<C-a><CR>"},
	{'<leader>z', t_esc .. ":lua require('fullscreen').toggle()<CR>i"},
	{'<C-h>', w_cmd .. "h"},
	{'<C-j>', w_cmd .. "j"},
	{'<C-k>', w_cmd .. "k"},
	{'<C-l>', w_cmd .. "l"},
	{'<M-h>', t_esc .. ":vertical resize +5<CR>i"},
	{'<M-j>', t_esc .. ":resize -5<CR>i"},
	{'<M-k>', t_esc .. ":resize +5<CR>i"},
	{'<M-l>', t_esc .. ":vertical resize -5<CR>i"},
}

local function set_tmaps(map_tbl)
	for _, map in pairs(map_tbl) do
		exec('au TermOpen * tnoremap <buffer> <silent> ' .. map[1] .. ' ' .. map[2])
		exec('au FileType fzf tunmap <buffer> ' .. map[1])
	end
end

function M.new(dir)
	exec(dirs_tbl[dir])
	for _, val in pairs(opts) do
		exec('setlocal ' .. val)
	end
	exec('startinsert')
end

exec("au BufEnter term://* startinsert")
set_tmaps(tmaps)

return M
