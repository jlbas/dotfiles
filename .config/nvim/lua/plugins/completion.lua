local H = require "helper"

local options = {
	completeopt = 'menuone,noinsert,noselect',
}

vim.g.completion_enable_snippet = 'vim-vsnip'

H.set_options(options)
