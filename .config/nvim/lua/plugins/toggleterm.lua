require"toggleterm".setup{
  size = 120,
  open_mapping = [[<c-\>]],
  shade_filetypes = {},
  shade_terminals = false,
  persist_size = false,
  direction = 'vertical',
}
