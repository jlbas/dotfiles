local lsp = require 'lspconfig'
local completion = require("completion")

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
	vim.lsp.diagnostic.on_publish_diagnostics, {
		underline = true,
		virtual_text = false,
		signs = true,
		update_in_insert = false,
	}
)

lsp.bashls.setup {
	cmd = { "bash-language-server", "start" },
	on_attach = completion.on_attach,
}

lsp.clangd.setup {
	on_attach = completion.on_attach
}

lsp.dockerls.setup {
	cmd = { "docker-langserver", "--stdio" },
	on_attach = completion.on_attach
}

lsp.gdscript.setup {
	on_attach = completion.on_attach
}

lsp.jsonls.setup {
	cmd = { "vscode-json-languageserver", "--stdio" },
	filestypes = { "json" },
	init_operations = { provideFormatter = true },
	on_attach = completion.on_attach
}

-- Couldn't get this to work
-- lsp.pyls.setup {
-- 	cmd = { "pyls" },
-- 	on_attach = completion.on_attach,
-- }

lsp.jedi_language_server.setup {
	cmd = { "jedi-language-server" },
	on_attach = completion.on_attach,
}

local sumneko_root = "/home/jl/src/lua-language-server-git/src/lua-language-server-git"
local sumneko_binary = sumneko_root.."/bin/Linux/lua-language-server"
lsp.sumneko_lua.setup {
	cmd = {sumneko_binary, "-E", sumneko_root .. "/main.lua"},
	settings = {
		Lua = {
			runtime = {
				version = 'LuaJIT',
				path = vim.split(package.path, ';'),
			},
			diagnostics = {
				globals = {'vim'},
			},
			workspace = {
				library = {
					[vim.fn.expand('$VIMRUNTIME/lua')] = true,
					[vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
				},
			},
		},
	},
	on_attach = completion.on_attach
}

lsp.texlab.setup {
	cmd = { "texlab" },
	settings = {
		latex = {
			build = {
				onSave = true
			},
			forwardSearch = {
				executable = "zathura",
				args = { "--synctex-forward", "%l:1:%f", "%p"},
				onSave = true
			},
		}
	},
	on_attach = completion.on_attach
}
