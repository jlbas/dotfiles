local H = require "helper"

require'nvim-treesitter.configs'.setup {
	ensure_installed = "maintained",
	highlight = {
		enable = true,
		disable = { },
	},
	indent = {
		enable = true,
	},
}

local options = {
	foldmethod = expr,
	foldexpr = 'nvim_treesitter#foldexpr()'
}

H.set_options(options)
