#!/usr/bin/env bash
# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# for m in $(xrandr | grep " connected" | grep -Eo "^[^ ]+"); do
#     MONITOR=$m polybar --reload bspwm &
# done

for m in $(bspc query -M --names); do
	if [[ $(xrandr | grep +1920 | grep $m) || $(xrandr | grep eDP | grep $m) ]]; then
		MONITOR=$m polybar -l warning --reload bspwm-main &
	else
		MONITOR=$m polybar -l warning --reload bspwm-side &
	fi
done

echo "Bars launched..."
