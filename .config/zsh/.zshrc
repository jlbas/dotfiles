#{{{ General Settings
# source dycol colors
. $HOME/.config/dycol/Generated/colors.sh
# Beware of below, slows down startup

# Completion
zstyle ':completion:*' completer _complete
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+l:|=* r:|=*'
zstyle ':completion:*' menu yes select
autoload -Uz compinit
compinit

# history
HISTFILE=$ZDOTDIR/.zhistory
HISTSIZE=10000
SAVEHIST=10000
setopt SHARE_HISTORY
setopt hist_save_no_dups
#}}}
#{{{ Autoquote pasting
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic

autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic
#}}}
#{{{ zplug configuration
source $ZDOTDIR/.zinit/bin/zinit.zsh
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
#}}}
#{{{ plugin section
# pure theme
zinit ice pick"async.zsh" src"pure.zsh"
zinit light sindresorhus/pure
zstyle :prompt:pure:execution_time       color $color3
zstyle :prompt:pure:git:arrow            color $color3
zstyle :prompt:pure:git:branch           color $color7
zstyle :prompt:pure:git:branch:cached    color $color7
zstyle :prompt:pure:git:action           color $color7
zstyle :prompt:pure:git:dirty            color $color3
zstyle :prompt:pure:host                 color $color15
zstyle :prompt:pure:path                 color $color15
zstyle :prompt:pure:prompt:error         color $color9
zstyle :prompt:pure:prompt:success       color $color3
zstyle :prompt:pure:prompt:continuation  color $color8
zstyle :prompt:pure:user                 color $color8
zstyle :prompt:pure:user:root            color $color8
zstyle :prompt:pure:virtualenv           color $color7

# vim mode
zinit load softmoth/zsh-vim-mode
MODE_CURSOR_VIINS='bar'

# autosuggestions
zinit light zsh-users/zsh-autosuggestions
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=$color7"
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
bindkey '^ ' autosuggest-accept

# Completion
zinit light zsh-users/zsh-history-substring-search
zinit light zsh-users/zsh-completions
CASE_SENSITIVE="false"

# Syntax
#zinit light zsh-users/zsh-syntax-highlighting
zinit light zdharma/fast-syntax-highlighting
#}}}
#{{{ tmux
#tmuxdir=$XDG_CONFIG_HOME/tmux/tmux.conf
#[[ $- != *i* ]] && return
#[[ -z "$TMUX" ]] && exec tmux -f $tmuxdir
#}}}
#{{{ fzf 
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
export FZF_COMPLETION_TRIGGER="\\"
export FZF_DEFAULT_COMMAND='fd --hidden --follow --exclude .git'
export FZF_DEFAULT_OPTS="--bind='ctrl-space:toggle-preview' --height 50% --reverse --border \
	--color fg:$color7,bg:$background,hl:$foreground,fg+:$color7,bg+:$color8,hl+:$color15 \
	--color info:$color7,prompt:$color3,spinner:$foreground,pointer:$color7,marker:$color7"
export FZF_ALT_C_COMMAND='fd -t d --hidden . $HOME'
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}
_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}
#}}}
#{{{ alias section
alias adbpush='adb push --sync /home/jl/Music/* /sdcard/Music'
alias bd='sudo screen_brightness down'
alias bu='sudo screen_brightness up'
alias color='msgcat --color=test'
alias config='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'
alias graph='git log --all --decorate --oneline --graph'
alias handout='h() { pdfjam --nup 2x3 --frame true --delta "0cm 1.5cm" --scale 0.95 "$1" --outfile "$2" }; h'
# alias la='ls -a'
# alias ll='ls -l'
# alias lla='ls -l -a'
# alias ls='ls --color=auto'
alias ls='exa --icons --git'
alias ll='exa -l --icons --git'
alias la='exa -a --icons --git'
alias lla='exa -la --icons --git'
alias m='matlab -nosplash -nodesktop'
alias pnv='pipenv run nvim'
alias q=exit
alias s="sxiv"
alias so="source $ZDOTDIR/.zshrc"
alias suckclean='make clean && rm -f config.h && git reset --hard origin/master'
alias suckinstall='make && sudo make clean install'
alias watvpn='sudo openconnect cn-vpn.uwaterloo.ca --no-dtls -b'
alias yt=youtube-viewer
alias v=nvim

alias ta='tmux attach -t'
alias td='tmux detach -t'
alias tad='tmux attach -d -t'
alias ts='tmux new-session -s'
alias tl='tmux list-sessions'
alias tksv='tmux kill-server -t'
alias tkss='tmux kill-session -t'
#}}}
#{{{ bindings
bindkey '\ex' fzf-cd-widget # This is because st vim_browser patch uses alt+c
zle -N fzf_pac
bindkey '^p' fzf_pac
zle -N fzf_yay
bindkey '^y' fzf_yay
zle -N goto_downloads
bindkey '^[d' goto_downloads
zle -N goto_papers
bindkey '^[p' goto_papers
zle -N goto_overleaf
bindkey '^[o' goto_overleaf
zle -N goto_research
bindkey '^[r' goto_research
#}}}
#{{{ functions
fzf_pac() { pacman -Slq | fzf --multi --preview 'cat <(pacman -Si {1}) <(pacman -Fl {1} | awk "{print \$2}")' | xargs -ro sudo pacman -S ; zle reset-prompt }
fzf_yay() { yay -Slq | fzf --multi --preview 'cat <(yay -Si {1}) <(yay -Fl {1} | awk "{print \$2}")' | xargs -ro yay -S ; zle reset-prompt }
goto_downloads() { cd ~/Downloads ; zle reset-prompt }
goto_papers() { cd ~/OneDrive/Papers ; zle reset-prompt }
goto_overleaf() { cd ~/OneDrive/Research/Overleaf ; zle reset-prompt }
goto_research() { cd ~/OneDrive/Research ; zle reset-prompt }
sysbackup() { sudo rsync -aAXv --delete --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found",".cache","Trash"} / /mnt/usb/arch_backup_incremental }
z() { nohup zathura "$1" > /dev/null 2>&1 &! }
zc() { nohup zathura "$1" > /dev/null 2>&1 &! exit }
play() { nohup mpv "$1" > /dev/null 2>&1 &! }
playc() { nohup mpv "$1" > /dev/null 2>&1 &! exit }
g() { g++ -g -Wall -Weffc++ -Wextra -Wsign-conversion -o ${1%.*} $1 }
#}}}
