#Prefix is Ctrl-a
set -g prefix C-a
bind C-a send-prefix
unbind C-b

set -g default-terminal screen-256color

# Prevent delay after pressing esc
set -sg escape-time 10

# Start indexing at 1
set -g base-index 1
setw -g pane-base-index 0

#Mouse works as expected
set -g mouse on

set -g mode-keys vi
set -g status-keys vi
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel
set -g aggressive-resize on
set -g history-limit 10000

bind space choose-session

# split panes like vim
bind s split-window -v
bind v split-window -h
unbind '"'
unbind %

# moving between windows with vim movement keys
bind -r h select-window -t :-
bind -r l select-window -t :+

# send pane to window
bind-key W command-prompt -p "send pane to:" "join-pane -t '%%'"

# resize panes with vim movement keys
bind -r < resize-pane -L 5
bind -r > resize-pane -R 5
bind -r + resize-pane -U 5
bind -r - resize-pane -D 5
bind = select-layout tiled

# show activity
setw -g monitor-activity on
set -g visual-activity on

# change resurrect bindings
set -g @resurrect-save 'S'
set -g @resurrect-restore 'R'

# resurrect
set -g @resurrect-capture-pane-contents 'on'
set -g @resurrect-strategy-nvim 'session'

# bind reload key
bind-key r source-file /home/jl/.config/tmux/tmux.conf \; display-message 'Config reloaded!'

set-window-option -g automatic-rename on
set-option -g set-titles on

# status bar
set-option -g status on
set-option -g status-position top
set-option -g status-style bg=colour0,fg=colour15
set-option -g status-justify left
set-window-option -g window-status-format " #I-#W "
set-window-option -g window-status-current-format " #I-#W "
set-window-option -g window-status-style bg=colour0,fg=colour15
set-window-option -g window-status-activity-style bg=colour3,fg=colour0
set-window-option -g window-status-current-style bg=colour15,fg=colour0
set-option -g pane-active-border-style fg=colour15
set-option -g pane-border-style fg=colour8
set-option -g message-style bg=colour15,fg=colour0
set-option -g message-command-style bg=colour15,fg=colour0
set -g status-right "[#S]"
set -g status-left ""

# Smart pane switching with awareness of vim and fzf
forward_programs="view|n?vim?|fzf"

should_forward="ps -o state= -o comm= -t '#{pane_tty}' \
  | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?($forward_programs)(diff)?$'"

bind -n C-h if-shell "$should_forward" "send-keys C-h" "select-pane -L"
bind -n C-j if-shell "$should_forward" "send-keys C-j" "select-pane -D"
bind -n C-k if-shell "$should_forward" "send-keys C-k" "select-pane -U"
bind -n C-l if-shell "$should_forward" "send-keys C-l" "select-pane -R"
bind -n C-\\ if-shell "$should_forward" "send-keys C-\\" "select-pane -l"

set -g @yank_selection 'primary'

set-environment -g TMUX_PLUGIN_MANAGER_PATH /home/jl/.config/tmux/plugins
# List of plugins
set -g @tpm_plugins ' 				\
	tmux-plugins/tpm 				\
	sainnhe/tmux-fzf 				\
	tmux-plugins/tmux-resurrect 	\
'

# Initialize tpm
run -b /home/jl/.config/tmux/plugins/tpm/tpm
