# Move zdotdir to .config
export ZDOTDIR="$HOME/.config/zsh"

# Font scaling for different monitors (fixes Alacritty rendering)
export WINIT_X11_SCALE_FACTOR=1

# Used for bat
# export:COLORTERM='truecolor'

# Better icon spacing
# export EXA_ICON_SPACING=2

# Used for bspwm
export XDG_CONFIG_HOME="$HOME/.config"

# Make nvim the default editor
export EDITOR=nvim
export VISUAL=nvim

# Prompt for password in dmenu
export SUDO_ASKPASS="$HOME/.local/bin/dpass"

# Adds '~/.local/bin' to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"

# For Matlab to work in DWM
export _JAVA_AWT_WM_NONREPARENTING=1

# Alacritty scaling
export WINIT_HIDPI_FACTOR=1

# Uniform qt and gtk themes
# export THEME=dycol
export QT_QPA_PLATFORMTHEME="qt5ct"
